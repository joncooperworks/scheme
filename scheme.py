import math
import operator



class SchemeError(Exception):
    """Base Exception."""

class SchemeSyntaxError(SchemeError):
    pass

class SchemeNameError(SchemeError):
    pass


class Env(dict):
    def __init__(self, names=(), values=(), parent=None):
        self.update(zip(names, values))
        self.parent = parent

    def find(self, name):
        if name in self:
            return self

        elif self.parent is not None:
            return self.parent.find(name)

        else:
            raise NameError('NameError: Undefined variable \'{0}\''.format(name))

    def flatten(self):
        """Flatten the environment.
        Compresses all the frames into one dict to be passed to Python.
        Names in the outermost scope will overwrite names in the inner scopes."""

        if self.parent is not None:
            self.update(self.parent.flatten())

        return self


class String(object):
    """Wrapper around strings."""

    def __init__(self, value):
        self.value = value


def populate_builtins(env):
    """Add builtin operators to environment."""

    def python_call(source, env):
        return eval(source, globals(), env.flatten())

    env.update(vars(math))
    env.update({
        '+': operator.add,
        '-': operator.sub,
        '*': operator.mul,
        '/': operator.truediv,
        'not': operator.not_,
        '>': operator.gt,
        '<': operator.lt,
        '>=': operator.ge,
        '<=': operator.le,
        '=': operator.eq,
        'equal?': operator.eq,
        'eq?': operator.is_,
        'length': len,
        'cons': lambda x, y: [x] + y,
        'car': lambda x: x[0],
        'cdr': lambda x: x[1],
        'append': operator.add,
        'list': lambda *x: list(x),
        'list?': lambda x: isinstance(x, list),
        'null?': lambda x: x == [],
        'symbol?': lambda x: isinstance(x, str),
        'call-python': lambda x: python_call(x, env)
    })
    return env


def evaluate(form, env):
    if isinstance(form, str):
        return env.find(form)[form]

    elif isinstance(form, String):
        return ''.join(form.value)

    elif not isinstance(form, list):
        return form

    elif form[0] == 'quote':
        _, exp = form
        return exp

    elif form[0] == 'if':
        _, test, conseq, alt = form
        return evaluate((conseq if evaluate(test, env) else alt), env)

    elif form[0] == 'set!':
        _, name, exp = form
        env.find(name)[name] = evaluate(exp, env)

    elif form[0] == 'def':
        _, name, exp = form
        env[name] = evaluate(exp, env)

    elif form[0] == 'lambda':
        _, vars, exp = form
        return lambda *args: evaluate(exp, Env(vars, args, env))

    elif form[0] == 'begin':
        val = None
        for exp in form[1:]:
            val = evaluate(exp, env)

        return val

    else:
        exps = [evaluate(exp, env) for exp in form]
        proc = exps.pop(0)
        return proc(*exps)


def read(s):
    return read_from(tokenize(s))


def tokenize(s):
    return s.replace('(', ' ( ').replace(')', ' ) ').split()


def read_from(tokens):
    if len(tokens) == 0:
        raise SchemeSyntaxError('Unexpected EOF while reading.')

    token = tokens.pop(0)
    if token == '(':
        ls = []
        while tokens[0] != ')':
            ls.append(read_from(tokens))

        tokens.pop(0) # Remove ')'
        return ls

    elif token == ')':
        raise SchemeSyntaxError('Unexpected )')

    else:
        return atom(token)


def atom(token):
    if token[0] == '"':
        token = token[1:]
        chars = []
        for char in token:
            if char == '"':
                break

            chars.append(char)
        return String(chars)
    try:
        return int(token)

    except ValueError:
        try:
            return float(token)

        except ValueError:
            return str(token)


def to_string(exp):
    if isinstance(exp, list):
        return '(' + ' '.join(map(to_string, exp)) + ' )'

    return str(exp)


if __name__ == '__main__':
    print('Python Scheme')
    global_env = populate_builtins(Env())
    while True:
        val = None
        try:
            val = evaluate(read(input('> ')), global_env)

        except SchemeError as e:
            print(e.msg)

        else:
            if val is not None:
                print(to_string(val))